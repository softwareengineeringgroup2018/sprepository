package studyplaner;

import java.util.ArrayList;

public class ModulesView {
    private  String PageTitle ;
    private ArrayList<String> modules = new ArrayList<>();

/* 
    ArrayList<String> modules = new ArrayList<String>(){{
		   add("DSA");
		   add("SE");
		    }};
    */
    
public ModulesView(){
    PageTitle = null;
}
public void setPageTitle(String newPageTitle){
    this.PageTitle = newPageTitle;}

public String getPageTitle(){
    return this.PageTitle;
}

public ArrayList<String> viewModules(){
    return modules;
}

}
