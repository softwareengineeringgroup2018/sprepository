package studyplaner;

public class Student {
    private String name; 
    private String userID;
    private String password;
    
    //default constructor
   
public Student(){
    name = null;
    userID = null;
    password = null;
}

public Student(String mName, String mUserID, String mPassword){
    name = mName;
    userID = mUserID;
    password= mPassword;
}
 
public void setName(String newName){
    name = newName;
}
 
 public String getName(){
    return name;
}
    
public void setUserID(String newUserID){
    userID=newUserID;
}

public String getUserID(){
    return userID;
}

public void setPassword(String newPassword){
    password=newPassword;
}

public String getPassword(){
    return password;
}
    
}
