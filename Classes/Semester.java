package studyplaner;

import java.util.Date;

public class Semester {

    private Date startDate = new Date();
    private Date endDate = new Date();

    private String name;
    private int milestones;
    private double progress;

//default constructor
    public Semester() {
        startDate = null;
        endDate = null;
        name = null;
        milestones = 0;
        progress = 0;
    }

    public void setName(String newName) {
        name = newName;
    }

    public String getName() {
        return name;
    }

    public void setMilestones(int newMilestones) {
        milestones = newMilestones;
    }

    public int getMilestones() {
        return milestones;
    }

    public void setProgress(Double newProgress) {
        progress = newProgress;
    }

    public double getProgress() {
        return progress;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

}
