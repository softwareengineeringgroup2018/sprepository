package studyplaner;

import java.util.ArrayList;

public class ModuleView {
    
    private  String module ;
    private  String description ;
    private  int progress;
    private ArrayList<String> Deadlines = new ArrayList<>();
    private ArrayList<String> TasksCompleted = new ArrayList<>();
    
    
    //default constructor
public ModuleView(){
    module = null;
    description = null;
    progress = 0;
    
}
public ModuleView(String mModule, String info, int mProgress){
    module = mModule;
    description = info;
    progress = mProgress;
}

public void setModule(String newModule){
    module = newModule;
}
public void setDescription(String newDescription){
    description = newDescription;

}
public void setProgress(int newProgress){
    progress = newProgress;
}

public String getModule(){
    return module;
}
public String getDescription(){
    return description;
}
public int DisplayProgress(){
    return progress;
}

}