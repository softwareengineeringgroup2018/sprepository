package studyplaner;

import java.util.ArrayList;

public class AssigmentView {
    private  String assigment ;
    private  String deadline ;
    private  int weighting;
    private  int progress;
    private ArrayList<String> StudyTask = new ArrayList<>();
    
    
        //default constructor
public AssigmentView(){
    assigment = null;
    deadline = null;
    weighting=0;
    progress = 0;
    
}
public AssigmentView(String mAssigment, String mDeadline,int mWeighting,
        int mProgress){
    assigment = mAssigment;
    deadline = mDeadline;
    weighting= mWeighting;
    progress = mProgress;
}
    
    public void setAssigment(String newAssigment){
    assigment = newAssigment;
}
public void setDeadline(String newDeadline){
    deadline = newDeadline;

}
public void setProgress(int newProgress){
    progress = newProgress;
}

public void setWeighting(int newWeighting){
    weighting = newWeighting;
}


public String getAssigment(){
    return assigment;
}
public String getDeadline(){
    return deadline;
}

public int getWeighting(){
    return weighting;
}

public int getProgress(){
    return progress;
}

public void AddStudyTask(){
        StudyTask.add("Reading");
        StudyTask.add("Coursework");
        StudyTask.add("Blogspot");
}
    
    
    
    
    
    
    
}

