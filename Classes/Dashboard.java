package studyplaner;

import java.util.ArrayList;

public class Dashboard {
    private  String message ;
    private final ArrayList<String> StudyTask = new ArrayList<>();
    private final ArrayList<String> UpcomingDeadlines= new ArrayList<>();
    private int progress;
    
        //default constructor
public Dashboard(){
    message = null;
    progress = 0;
} 

public Dashboard(String mMessage ,int mProgress){
    message = mMessage;
    progress = mProgress;
}

public void setMessage(String newMessage){
    message = newMessage;
}

public String getMessage(){
    return message;
}

public void setProgress(int newProgress){
    progress = newProgress;
}

public int getProgress(){
    return progress;
}
public void CalculateProgress(){
    progress=progress+this.progress;
}

}
