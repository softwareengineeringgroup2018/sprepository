package studyplaner;

public class Study {
   private String type; 
   private int timeRequired;
   private int priority ;

//default constructor
   
public Study(){
    type = null;
    timeRequired = 0;
    priority=0;
}

public Study(String mType,int mTimeRequired,int mPriority)
{
    type = mType;
    timeRequired = mTimeRequired;
    priority= mPriority;
}


 public void setType(String newType){
    type = newType;
}
 
 public String getType(){
    return type;
}

 public void setTimeRequired(int newTimeRequired){
     timeRequired = newTimeRequired;
 }
 
 public int getTimeRequired(){
     return timeRequired;
 }
 
 public void setPriority(int newPriority){
     priority = newPriority;
 }
 
public int getPriority(){
    return priority;
}

}
