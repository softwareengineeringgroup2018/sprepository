<%-- 
    Document   : contact
    Created on : 02-May-2018, 12:28:58
    Author     : ngx16ybu
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="javax.servlet.RequestDispatcher"%>
<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
    <head>


        <title>Contact Us</title>
        <!-- Main CSS Stylesheet for website -->
        <link rel="stylesheet" href="resources/css/styles.css" />
        <!-- CSS Grid Stylesheet, Available at: https://thisisdallas.github.io/Simple-Grid/ -->
        <link rel="stylesheet" href="resources/css/simplegrid.css" />
        <!-- Bootstrap CDN, Available at: https://v4-alpha.getbootstrap.com/getting-started/download/#package-managers -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous" />
        <!-- Form JavaScript File -->

        <script src="resources/js/form.js"></script>
        <!-- Main JavaScript File -->

        <script src="resources/js/main.js"></script>
        <!-- jQuery CDN, Available at: http://www.w3schools.com/jquery/jquery_get_started.asp -->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script></head>
    <body>
        <%

            if (session.getAttribute("name") == null) {
                response.sendRedirect("login.jsp");
            }

        %>
        <header>

            <div id="logo-block">
                <img src="resources/img/logo1.png" alt="Study Planner" class="logo">
            </div>
            <!--<button class="nav-menu-btn"><img src="img/menu.svg" alt="Menu">Menu</button>-->
            <nav id="navigation">

                <a href="index.jsp">Home</a>
                <a href="modules.jsp">Modules</a>
                <a href="tasks.jsp">Tasks</a>
                <a href="progress.jsp">Progress</a>
                <a href="uploadPage.jsp">Upload</a>
                <a class="current" href="contact.jsp">Contact Us</a>
                <a href="LogoutServlet">Logout</a>

            </nav>
        </header>

        <div class="page-hero page-hero-contact">

            <div class="page-hero-content">

                <h1>Get In Touch With Us</h1>
            </div>
        </div>

        <div class="content-wrapper" id="contact">

            <div class="col-1-2">
                <form class="form-wrap">
                    <label>First Name</label>
                    <input type="text" placeholder="Please Enter Your First Name" />
                    <label>Surname</label>
                    <input type="text" placeholder="Please Enter Your Surname" />
                    <label>Email Address</label>
                    <input type="text" placeholder="I.e. johnsmith@email.com" />
                    <label>Telephone Number</label>
                    <input type="text" placeholder="I.e. 03454834734" />
                    <label class="msg">Message</label>
                    <textarea name="message" id="message" placeholder="Please Enter Your Message"></textarea>
                    <button type="submit">Submit</button>
                </form>
            </div>

            <div class="devon-map col-1-2">


                <h3>Support Team Member</h3>

                <p>Zack Shatten</p>

                <h3>Email Address</h3>

                <p>z.shatten@uea.ac.uk</p>

                <h3>Telephone Number</h3>

                <p>074 24453 676</p>

                <h3>Address</h3>

                <p>
                    Norwich Research Park, 
                    Norwich, <br />
                    Norfolk. <br />
                    NR4 7TJ <br />
                </p>

                <h3>About Agent</h3>

                <p>Zack has been one of UEA's most renowned agents and has worked for us since 1997 and continues to be a promising asset to our team.</p>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2422.043702963815!2d1.24131535!3d52.62305435!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb9b97bf00744e573!2sUniversity+of+East+Anglia!5e0!3m2!1sen!2suk!4v1526389546565" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </body></html>
