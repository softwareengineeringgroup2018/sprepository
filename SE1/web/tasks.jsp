<%-- 
    Document   : gallery
    Created on : 02-May-2018, 12:31:15
    Author     : ngx16ybu
--%>

<%@page import="java.util.List"%>
<%@page import="studyplanner.Task"%>
<%@page import="controller.DatabaseController"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="javax.servlet.RequestDispatcher"%>
<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
    <head>


        <title>Tasks</title>
        <!-- Main CSS Stylesheet for website -->
        <link rel="stylesheet" href="resources/css/styles.css" />
        <!-- CSS Grid Stylesheet, Available at: https://thisisdallas.github.io/Simple-Grid/ -->
        <link rel="stylesheet" href="resources/css/simplegrid.css" />
        <!-- Bootstrap CDN, Available at: https://v4-alpha.getbootstrap.com/getting-started/download/#package-managers -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous" />
        <!-- Form JavaScript File -->

        <script src="resources/js/form.js"></script>
        <!-- Main JavaScript File -->

        <script src="resources/js/main.js"></script>
        <!-- jQuery CDN, Available at: http://www.w3schools.com/jquery/jquery_get_started.asp -->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script></head>
    <body>


        <header>

            <div id="logo-block">
                <img src="resources/img/logo1.png" alt="Study Planner" class="logo">
            </div>
            <!--<button class="nav-menu-btn"><img src="img/menu.svg" alt="Menu">Menu</button>-->
            <nav id="navigation">

                <a href="index.jsp">Home</a>
                <a href="modules.jsp">Modules</a>
                <a class="current" href="tasks.jsp">Tasks</a>
                <a href="progress.jsp">Progress</a>
                <a href="uploadPage.jsp">Upload</a>
                <a href="contact.jsp">Contact Us</a>
                <a href="LogoutServlet">Logout</a>

            </nav>
        </header>

        <div class="page-hero page-hero-gallery">

            <div class="page-hero-content">
                <form method="post" action ="TaskController">		
                    <h1>Tasks</h1>
                    <center>
                        <table style="background:snow;" border="1" cellpadding="5" cellspacing="2">
                            <thead>
                                <tr>
                                    <th colspan="2">Create Account</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Task Name</td>
                                    <td><input type="text" name="taskname"/></td>
                                </tr>
                                <tr>
                                    <td>Description</td>
                                    <td><input type="text" name="description"</td>
                                </tr>
                                <tr>
                                    <td>Duration</td>
                                    <td><input type="text" name="duration" /></td>
                                </tr>
                                <tr>
                                    <td>Priority</td>
                                    <td><input type="text" name="priority" /></td>
                                </tr>
                                <tr>
                                    <td>Module</td>
                                    <td><input type="text" name="module" /></td>
                                </tr>
                                <tr>
                                    <td>Assignment</td>
                                    <td><input type="text" name="assignment" /></td>
                                </tr>

                                <tr>
                                    <td colspan="2" align="center"><input type="submit" value="Submit Task" />
                                        &nbsp;&nbsp;
                                        <input type="reset" value="Reset" />
                                    </td>                        
                                </tr>                    
                            </tbody>
                        </table>
                    </center>
                </form>
            </div>





            <style>
                body {
                    margin: 0;
                    min-width: 250px;
                }

                /* Include the padding and border in an element's total width and height */
                * {
                    box-sizing: border-box;
                }

                /* Remove margins and padding from the list */
                ul {
                    margin: 0;
                    padding: 0;
                }

                /* Style the list items */
                ul li {
                    cursor: pointer;
                    position: relative;
                    padding: 12px 8px 12px 40px;
                    list-style-type: none;
                    background: #eee;
                    font-size: 18px;
                    transition: 0.2s;

                    /* make the list items unselectable */
                    -webkit-user-select: none;
                    -moz-user-select: none;
                    -ms-user-select: none;
                    user-select: none;
                }

                /* Set all odd list items to a different color (zebra-stripes) */
                ul li:nth-child(odd) {
                    background: #f9f9f9;
                }

                /* Darker background-color on hover */
                ul li:hover {
                    background: #ddd;
                }

                /* When clicked on, add a background color and strike out text */
                ul li.checked {
                    background: #888;
                    color: #fff;
                    text-decoration: line-through;
                }

                /* Add a "checked" mark when clicked on */
                ul li.checked::before {
                    content: '';
                    position: absolute;
                    border-color: #fff;
                    border-style: solid;
                    border-width: 0 2px 2px 0;
                    top: 10px;
                    left: 16px;
                    transform: rotate(45deg);
                    height: 15px;
                    width: 7px;
                }

                /* Style the close button */
                .close {
                    position: absolute;
                    right: 0;
                    top: 0;
                    padding: 12px 16px 12px 16px;
                }

                .close:hover {
                    background-color: #f44336;
                    color: white;
                }

                /* Style the header */
                .header {
                    background-color: #f44336;
                    padding: 30px 40px;
                    color: white;
                    text-align: center;
                }

                /* Clear floats after the header */
                .header:after {
                    content: "";
                    display: table;
                    clear: both;
                }

                /* Style the input */
                input {
                    border: none;
                    width: 75%;
                    padding: 10px;
                    float: left;
                    font-size: 16px;
                }

                /* Style the "Add" button */
                .addBtn {
                    padding: 10px;
                    width: 25%;
                    background: #d9d9d9;
                    color: #555;
                    float: left;
                    text-align: center;
                    font-size: 16px;
                    cursor: pointer;
                    transition: 0.3s;
                }

                .addBtn:hover {
                    background-color: #bbb;
                }
            </style>
            </head>
            <body>

                <div id="myDIV" class="header">
                    <h2 style="margin:5px">My To Do List</h2>
                    <input type="text" id="myInput" placeholder="Title...">
                        <span onclick="newElement()" class="addBtn">Add</span>
                </div>


                <ul id="myUL">
                    <select>
                        <%  if (session.getAttribute("name") == null) {
                                response.sendRedirect("login.jsp");
                            } else {
                                String userID = session.getAttribute("name").toString();
                                DatabaseController db = new DatabaseController();
                                studyplanner.StudentProfile stu = db.readStudentProfile(userID);
                                stu.generateTasks();
                                db.updateStudentProfile(userID, stu);

                                List<Task> t = stu.allTasksToDo();
                                for (Task d : t) { %> 


                        <option value =<%out.println(d);%>><%out.println(d);%></option>

                        <% }
                            }%>
                    </select>
                </ul>   





        </div>
        <script>
            // Create a "close" button and append it to each list item
            var myNodelist = document.getElementsByTagName("LI");
            var i;
            for (i = 0; i < myNodelist.length; i++) {
                var span = document.createElement("SPAN");
                var txt = document.createTextNode("\u00D7");
                span.className = "close";
                span.appendChild(txt);
                myNodelist[i].appendChild(span);
            }

            // Click on a close button to hide the current list item
            var close = document.getElementsByClassName("close");
            var i;
            for (i = 0; i < close.length; i++) {
                close[i].onclick = function () {
                    var div = this.parentElement;
                    div.style.display = "none";
                }
            }

            // Add a "checked" symbol when clicking on a list item
            var list = document.querySelector('ul');
            list.addEventListener('click', function (ev) {
                if (ev.target.tagName === 'LI') {
                    ev.target.classList.toggle('checked');
                }
            }, false);

            // Create a new list item when clicking on the "Add" button
            function newElement() {
                var li = document.createElement("li");
                var inputValue = document.getElementById("myInput").value;
                var t = document.createTextNode(inputValue);
                li.appendChild(t);
                if (inputValue === '') {
                    alert("You must write something!");
                } else {
                    document.getElementById("myUL").appendChild(li);
                }
                document.getElementById("myInput").value = "";

                var span = document.createElement("SPAN");
                var txt = document.createTextNode("\u00D7");
                span.className = "close";
                span.appendChild(txt);
                li.appendChild(span);

                for (i = 0; i < close.length; i++) {
                    close[i].onclick = function () {
                        var div = this.parentElement;
                        div.style.display = "none";
                    }
                }
            }
        </script>

        </div>
    </body></html>
