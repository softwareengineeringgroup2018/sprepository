
<%@page import = "studyplanner.StudentProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    	<!-- Main CSS Stylesheet for website -->
	<link rel="stylesheet" href="resources/css/styles.css" />
	<!-- CSS Grid Stylesheet, Available at: https://thisisdallas.github.io/Simple-Grid/ -->
	<link rel="stylesheet" href="resources/css/simplegrid.css" />
	<!-- Bootstrap CDN, Available at: https://v4-alpha.getbootstrap.com/getting-started/download/#package-managers -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha.384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous" />
	<!-- Form JavaScript File -->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Create Account</title>
    </head>
    <body>
        <div id="logo-block">
			<img src="resources/img/logo1.png" alt="Study Planner" class="logo">
		</div>
       <div class="hero-wrapper">
		
<div  class="page-hero-content">
			
         <title>Create Account</title>
    </head>
    <body>
       
       <form method="post" action="RegistrationController">
            <center>
                <table style="background:snow;" border="1" cellpadding="5" cellspacing="2">
                    <thead>
                        <tr>
                            <th colspan="2">Create Account</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Username</td>
                            <td><input type="text" name="username" required pattern="[A-Za-z]{3}[0-9]{2}[A-Za-z]{3}" title="Must have three letters, two digits and three letters"/></td>
                        </tr>
                        <tr>
                            <td>Password</td>
                            <td><input type="password" name="password" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"/></td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td><input type="email" name="email" required/></td>
                        </tr>
                        <tr>
                            <td>First Name</td>
                            <td><input type="firstname" name="firstname" required pattern="[A-Za-z]{20}" title="Max 20 letters name"/></td>
                        </tr>
                        <tr>
                            <td>Last Name</td>
                            <td><input type="lastname" name="lastname" required pattern="[A-Za-z]{20}" title="Max 20 letters name"/></td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center"><input type="submit" value="Create Account!" />
                                &nbsp;&nbsp;
                                <input type="reset" value="Reset" />
                            </td>                        
                        </tr>                    
                    </tbody>
                </table>
            </center>
        </form>
		</div>
	</div>
        
 

			
    	

    </body>
</html>

