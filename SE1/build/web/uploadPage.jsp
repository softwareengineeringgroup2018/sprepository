<%-- 
    Document   : attractions
    Created on : 02-May-2018, 12:24:02
    Author     : ngx16ybu
--%>
<%@page import="com.oreilly.servlet.MultipartRequest"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.io.*" %>"
<%@page import="javax.servlet.RequestDispatcher"%>
<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
    <head>

        <title>Upload</title>
        <!-- Main CSS Stylesheet for website -->
        <link rel="stylesheet" href="resources/css/styles.css" />
        <!-- CSS Grid Stylesheet, Available at: https://thisisdallas.github.io/Simple-Grid/ -->
        <link rel="stylesheet" href="resources/css/simplegrid.css" />
        <!-- Bootstrap CDN, Available at: https://v4-alpha.getbootstrap.com/getting-started/download/#package-managers -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous" />
        <!-- Form JavaScript File -->

        <script src="resources/js/form.js"></script>
        <!-- Main JavaScript File -->

        <script src="resources/js/main.js"></script>
        <!-- jQuery CDN, Available at: http://www.w3schools.com/jquery/jquery_get_started.asp -->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script></head>
    <body>
        <%
            if (session.getAttribute("name") == null) {
                response.sendRedirect("login.jsp");
            }
        %>
        <title>Upload</title>
        <header>

            <div id="logo-block">
                <img src="resources/img/logo1.png" alt="Study Planner" class="logo">
            </div>
            <!--<button class="nav-menu-btn"><img src="img/menu.svg" alt="Menu">Menu</button>-->
            <nav id="navigation">

                <a href="index.jsp">Home</a>
                <a href="modules.jsp">Modules</a>
                <a href="tasks.jsp">Tasks</a>
                <a href="progress.jsp">Progress</a>
                <a  class="current" href="uploadPage.jsp">Upload</a>
                <a href="contact.jsp">Contact Us</a>
                <a href="LogoutServlet">Logout</a> 

            </nav>
        </header>

        <div class="page-hero page-hero-upload">

            <div class="page-hero-content">

                <h1>Upload</h1>
            </div>
        </div>
        <center
            File: <br />
            <form name="uploadForm" action="uploadPage.jsp" method="POST" enctype="multipart/form-data">
                <%
                    String saveFile = new String();
                    String contentType = request.getContentType();

                    if ((contentType != null) && (contentType.indexOf("multipart/form-data") >= 0)) {
                        DataInputStream in = new DataInputStream(request.getInputStream());
                        int formDataLength = request.getContentLength();
                        byte dataBytes[] = new byte[formDataLength];
                        int byteRead = 0;
                        int totalBytesRead = 0;

                        while (totalBytesRead < formDataLength) {
                            byteRead = in.read(dataBytes, totalBytesRead, formDataLength);
                            totalBytesRead += byteRead;
                        }
                        String file = new String(dataBytes);
                        saveFile = file.substring(file.indexOf("filename=\"") + 10);
                        saveFile = saveFile.substring(0, saveFile.indexOf("\n"));
                        saveFile = saveFile.substring(saveFile.lastIndexOf("\\") + 1, saveFile.indexOf("\""));
                        int lastIndex = contentType.lastIndexOf("=");

                        String boundary = contentType.substring(lastIndex + 1, contentType.length());
                        int pos;
                        pos = file.indexOf("filename=\"");
                        pos = file.indexOf("\n", pos) + 1;
                        pos = file.indexOf("\n", pos) + 1;
                        pos = file.indexOf("\n", pos) + 1;

                        int boundaryLocation = file.indexOf(boundary, pos) - 4;

                        int startPos = ((file.substring(0, pos)).getBytes().length);
                        int endPos = ((file.substring(0, boundaryLocation)).getBytes().length);

                        saveFile = "//ueahome4/stusci1/ngx16ybu/data/NTProfile/Desktop/SOFTWARE/SE1/upload" + saveFile;
                        File ff = new File(saveFile);
                        try {
                            FileOutputStream fileOut = new FileOutputStream(ff);
                            fileOut.write(dataBytes, startPos, (endPos - startPos));

                            fileOut.flush();

                            fileOut.close();
                            out.println("File has been succesfully  uploaded");
                        } catch (Exception e) {
                            out.println("Please attach a file before submitting it");
                        }
                    }

                %> 
                <center>
                    <label style="border: 1px solid #ccc;
                           display: inline-block;
                           padding: 6px 12px;
                           cursor: pointer;
                           width:20%;
                           background:#ccc;" class="custom-file-upload">
                        <input style="display: none;" type="file" name="file"/>
                        <i class="fa fa-cloud-upload"></i> Choose File...
                    </label>
                </center>
                <br />
                <input style="padding:5px 15px; background:#ccc; border:0 none;
                       cursor:pointer;
                       -webkit-border-radius: 5px;
                       border-radius: 5px;" type="submit" value="Submit" name="Submit" />

            </form>






            <%-- emil upload
            <form action="upload.jsp" method="post" enctype="multipart/form-data">  
                Select File:
                <input type="file" name="fname"/><br/>  

                <input type="submit" value="upload" name="upload" />  
                                
            </form>
            --%>

        </center>
    </body></html>
