<%-- 
    Document   : reviews
    Created on : 02-May-2018, 12:31:36
    Author     : ngx16ybu
--%>

<%@page import="studyplanner.Assignment"%>
<%@page import="controller.DatabaseController"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="javax.servlet.RequestDispatcher"%>
<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
    <head>


        <title>Progress</title>
        <!-- Main CSS Stylesheet for website -->
        <link rel="stylesheet" href="resources/css/styles.css" />
        <!-- CSS Grid Stylesheet, Available at: https://thisisdallas.github.io/Simple-Grid/ -->
        <link rel="stylesheet" href="resources/css/simplegrid.css" />
        <!-- Bootstrap CDN, Available at: https://v4-alpha.getbootstrap.com/getting-started/download/#package-managers -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous" />
        <!-- Form JavaScript File -->

        <script src="resources/js/form.js"></script>
        <!-- Main JavaScript File -->

        <script src="resources/js/main.js"></script>
        <!-- jQuery CDN, Available at: http://www.w3schools.com/jquery/jquery_get_started.asp -->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script></head>
    <body onload="fetchComments()">


        <header>

            <div id="logo-block">
                <img src="resources/img/logo1.png" alt="Study Planner" class="logo">
            </div>
            <!--<button class="nav-menu-btn"><img src="img/menu.svg" alt="Menu">Menu</button>-->
            <nav id="navigation">

                <a href="index.jsp">Home</a>
                <a href="modules.jsp">Modules</a>
                <a href="tasks.jsp">Tasks</a>
                <a class="current" href="progress.jsp">Assignments</a>
                <a  href="uploadPage.jsp">Upload</a>
                <a href="contact.jsp">Contact Us</a>
                <a href="LogoutServlet">Logout</a>

            </nav>
        </header>

        <div class="page-hero page-hero-reviews">

            <div class="page-hero-content">

                <h1>Assignments</h1>
            </div>
        </div>

        <div class="content-wrapper">





            <%
                if (session.getAttribute("name") == null) {
                    response.sendRedirect("login.jsp");
                } else {

                    String userID = session.getAttribute("name").toString();
                    DatabaseController db = new DatabaseController();
                    studyplanner.StudentProfile stu = db.readStudentProfile(userID);
            %> 
            <tr>
                <%
                    for (int i = 0; i < stu.getModules().size(); i++) {
                        studyplanner.Module M = stu.getModules().get(i);
                %>
            </tr>
            <div class="fac-grid">
                <div class="fac-facility">


                    <div class="col-1-3 fac-block">
                        <% out.println(M.getName() + " - " + M.getCode() + "<br>");
                                    out.println("Assignments: <br>");
                                    for (Assignment A : M.getAssignments()) {
                                        out.println(A.getName() + "<br>");

                                    }
                                }

//            stu.generateTasks();
//            db.updateStudentProfile(userID, stu);
//            List<Task> t = stu.allTasksToDo();
//            for(Task d: t) {
//                out.println(d.ToString());
//            }
                            }%>
                    </div>



                </div>
            </div>

    </body></html>