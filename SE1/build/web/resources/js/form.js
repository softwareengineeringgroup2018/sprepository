// Comments

/*

// HIS =============================================
//clears all the comments
function clearComment(){
	$('#txt1').val('');
	$('#namebox').val('');
};


//this will save the comments
function saveComment(){
	
	var ctext = $('#txt1').val();
	var cname = $('#namebox').val();
	var cmtlist = $('#cmtlist').html();
	if (cname === 'name'){cname = 'Anon';}
	alert('saveComment cname=' + cname + ' ctext=' + ctext);

	var prevComments = $('#cmtlist').html();
	var curComment='<p style="font-weight:bold"><span class="cmtname" '+cname
	+':'+'</span>' + cname +'</p> <p>' + ctext + '</p>';

	setObject('totCmts', cmtlist);
	$('#cmtlist').empty();
	$('#cmtlist').append(curComment);
	$('#cmtlist').append(prevComment);
	clearComment();
};
	
	
function fetchComments(){
	var inlist=getObject('totCmts');

	if(inlist === null){
	inlist='';
	}

	//display the comments$
	$('#cmtlist').empty();
	$('#cmtlist').append(inlist);
};

*/

// =================================== NEW

function clearComment(){
	$('#txt1').val('');
	$('#namebox').val('');
	
	if ($('#txt1').val() == "CLEAR ALL STORAGE NOW!"){
		clearStorage();
		location.reload();
	};
};

function saveComment(){
	var ctext = $('#txt1').val();
	var cname = $('#namebox').val();
	if (ctext === ''){
		alert('You have not included a comment! Please enter one.');
		return;
	}

	if (cname === 'Name') {cname = 'Anon';}
	alert('Your comment has been submitted!');

	var date = new Date();
	var curComment= date+'<br />'+'<p><span class="cmtname">Name: '+cname+'</span><br />'+'Comment: '+ctext+'</p>';
	var prevComments = $('#cmtlist').html();
	var mainComment = curComment + prevComments; 
	$('#cmtlist').prepend(mainComment);
	//$('#cmtlist').append(mainComment);
	clearComment();
	var cmtlist = $('#cmtlist').html();
	setObject('All', cmtlist);
};

function setObject(key, value) {
	window.localStorage.setItem(key,JSON.stringify(value));
};
function getObject(key) {
	var storage = window.localStorage;
	var value = storage.getItem(key);
	return value && JSON.parse(value);
};
function fetchComments(){
	var inlist=getObject('All');
	if(inlist === null){
		inlist='';
	}

	//display the comments
	$('#cmtlist').empty();
	$('#cmtlist').append(inlist);
};
function clearStorage() {
	// removes everything placed in localstorage
	window.localStorage.clear();
};

// =================================== OLD
/*
function clearComment(){
	$("#txtarea").val('');
	$("#namebox").val('');
};

function saveComment(){  
	var ctext = $('#txtarea').val();
	var cname = $('#namebox').val();
	if (cname === 'Name') {cname = 'Anon';}
	alert('saveComment cname=' + cname + ctext=' +')

	var curComment='<p><span class="cmtname">'+cname+':'+'</span>'+ctext+'</p>';
	$('#cmtlist').empty();
	$('#cmtlist').append(curComment);

	var prevComments = $('#cmtlist').html();
	setObject('totCmts', cmtlist);
};

function fetchComments(){
	var inlist=getObject('totCmts');
	if(inlist === null){
		inlist='';
	}

	//display the comments
	$('#cmtlist').empty();
	$('#cmtlist').append(inlist);
};

if ($('#txt1').val() === 'CLEAR ALL STORAGE NOW!'){
	clearStorage();
};

// Local Storage
function setObject(key, value) {
	window.localStorage.setItem(key,
	JSON.stringify(value));
};

function getObject(key) {
	var storage = window.localStorage;
	var value = storage.getItem(key);
	return value && JSON.parse(value);
};

function clearStorage() {
	// removes everything placed in localstorage
	window.localStorage.clear();
};

*/