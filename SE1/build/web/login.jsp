
<%@page import = "studyplanner.StudentProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    	<!-- Main CSS Stylesheet for website -->
	<link rel="stylesheet" href="resources/css/styles.css" />
	<!-- CSS Grid Stylesheet, Available at: https://thisisdallas.github.io/Simple-Grid/ -->
	<link rel="stylesheet" href="resources/css/simplegrid.css" />
	<!-- Bootstrap CDN, Available at: https://v4-alpha.getbootstrap.com/getting-started/download/#package-managers -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha.384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous" />
	<!-- Form JavaScript File -->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
    </head>
    <body>
        <div id="logo-block">
			<img src="resources/img/logo1.png" alt="Study Planner" class="logo">
		</div>
       <div class="hero-wrapper">
		
<div  class="page-hero-content">
			
<form method="post" action="LoginController">
            <center>
            <table style="padding: 70px 0;border: 3px solid green;text-align: center;    background: snow;
    width: 323px;
    height: 174px" border="1" cellpadding="5" cellspacing="2">
                <thead>
                    <tr>
                        <th colspan="2">Login Here</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Username</td>
                        <td><input type="text" name="username" required pattern="[A-Za-z]{3}[0-9]{2}[A-Za-z]{3}" title="Must have three letters, two digits and three letters"/></td>
                    </tr>
                    <tr>
                        <td>Password</td>
                        <td><input type="password" name="password" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"/></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center"><input type="submit" value="Login" />
                            &nbsp;&nbsp;
                            <input type="reset" value="Reset" />
                        </td>                        
                    </tr>                    
                </tbody>
            </table> <h1 style="color:white;">Don't you have an account? Create one</h1>
	<a href="createaccount.jsp"><input type="button" name="createaccount" value="Create Account"></a>
            </center>
        </form>
		</div>
	</div>
    </body>
</html>