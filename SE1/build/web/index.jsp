<%-- 
    Document   : index
    Created on : 02-May-2018, 12:18:27
    Author     : ngx16ybu
--%>

<%@page import="studyplanner.Task"%>
<%@page import="java.util.List"%>
<%@page import="studyplanner.Assignment"%>
<%@page import="studyplanner.Assignment"%>
<%@page import="controller.DatabaseController"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.RequestDispatcher"%>
<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
    <head>


        <title>Home</title>
        <!-- Main CSS Stylesheet for website -->
        <link rel="stylesheet" href="resources/css/styles.css" />
        <!-- CSS Grid Stylesheet, Available at: https://thisisdallas.github.io/Simple-Grid/ -->
        <link rel="stylesheet" href="resources/css/simplegrid.css" />
        <!-- Bootstrap CDN, Available at: https://v4-alpha.getbootstrap.com/getting-started/download/#package-managers -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha.384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous" />
        <!-- Form JavaScript File -->

        <script src="resources/js/form.js"></script>
        <!-- Main JavaScript File -->

        <script src="resources/js/main.js"></script>
        <!-- jQuery CDN, Available at: http://www.w3schools.com/jquery/jquery_get_started.asp -->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script></head>
    <body>


        <header>

            <div id="logo-block">
                <img src="resources/img/logo1.png" alt="Study Planner" class="logo">
            </div>
            <!--<button class="nav-menu-btn"><img src="img/menu.svg" alt="Menu">Menu</button>-->
            <nav id="navigation">
                Welcome <%= session.getAttribute("name")%>

                <a class="current" href="index.jsp">Home</a>
                <a href="modules.jsp">Modules</a>
                <a href="tasks.jsp">Tasks</a>
                <a href="progress.jsp">Progress</a>
                <a href="uploadPage.jsp">Upload</a>
                <a href="contact.jsp">Contact Us</a>
                <a href="LogoutServlet">Logout</a>
            </nav>
        </header>

        <!--@{ViewBag.Title = "index";}
                <h2>HTML HELP</h2>
                @Html.DropDownList("Departments", new List<SelectListItem>
                    {
                    new SelectListItem {Text = "IT", value ="1", Selected = true},
                    new SelectListItem {Text = "HR", value ="2"},
                    new SelectListItem {Text = "Payroll", value ="3"},"Select Department")-->
        <div class="hero-wrapper">

            <div class="hero-info-wrapper">
                <%
                    if (session.getAttribute("name") == null) {
                        response.sendRedirect("login.jsp");
                    } else {

                        String userID = session.getAttribute("name").toString();
                        DatabaseController db = new DatabaseController();
                        studyplanner.StudentProfile stu = db.readStudentProfile(userID);
                        db.updateStudentProfile(userID, stu);

                        List<Assignment> a = stu.upcommingDeadlines();

                        out.println("Upcomming Deadlines");

                        for (int i = 0; i < 3; i++) {

                %>
                <ul>
                    <li><% out.println(a.get(i));

                        %></li></ul>
                        <% }
                            List<Task> t = stu.allTasksToDo();

                            out.println("Urgent Tasks");

                            for (int j = 0; j < 3; j++) {
                        %>
                <ul>
                    <li> 
                        <%
                                    out.println(t.get(j));
                                }
                            }%>
                    </li>

                </ul>











            </div>
        </div>
    </body></html>