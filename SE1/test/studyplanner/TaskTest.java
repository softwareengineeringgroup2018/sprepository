/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package studyplanner;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ging3
 */
public class TaskTest {
    
    public TaskTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getName and setName method, of class Task.
     */
    @Test
    public void testGetNameAndSetName() {
        System.out.println("getNameAndSetName");
        Task instance = new Task();
        instance.setName("test");
        if (instance.getName().equals("test")) {
            System.out.println("set and get tested succesfully");
        }
        else { 
            fail("The test case is a prototype.");
        }
        
    }

    /**
     * Test of getType method, of class Task.
     */
    @Test
    public void testGetTypeAndSetType() {
        System.out.println("getTypeAndSetType");
        Task instance = new Task();
        instance.setType("test");
        if (instance.getType().equals("test")) {
            System.out.println("set and get tested succesfully");
        }
        else { 
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getTimeRequired method, of class Task.
     */
    @Test
    public void testGetTimeRequiredAndSetTimeRequired() {
        System.out.println("getTimeRequiredAndSetTimeRequired");
        Task instance = new Task();
        instance.setTimeRequired(10);
        if (instance.getTimeRequired() == 10) {
            System.out.println("set and get tested succesfully");
        }
        else { 
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getPriority method, of class Task.
     */
    @Test
    public void testGetPriorityAndSetPriority() {
        System.out.println("getPriorityAndSetPriority");
        Task instance = new Task();
        instance.setPriority(5);
        if (instance.getPriority() == 5) {
            System.out.println("set and get tested succesfully");
        }
        else { 
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getCompletion method, of class Task.
     */
    @Test
    public void testGetCompletion() {
        System.out.println("getCompletion");
        Task instance = new Task();
        if (instance.getCompletion() == false) {
            System.out.println("get tested succesfully");
        }
        else { 
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of CompleteTask method, of class Task.
     */
    @Test
    public void testCompleteTask() {
        System.out.println("CompleteTask");
        Task instance = new Task();
        instance.CompleteTask();
  
        if (instance.getCompletion() == true) {
            System.out.println("set tested succesfully");
        }
        else { 
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of UndoCompleteTask method, of class Task.
     */
    @Test
    public void testUndoCompleteTask() {
        System.out.println("UndoCompleteTask");
        Task instance = new Task();
        instance.UndoCompleteTask();
  
        if (instance.getCompletion() == false) {
            System.out.println("set tested succesfully");
        }
        else { 
            fail("The test case is a prototype.");
        }
    }
}
