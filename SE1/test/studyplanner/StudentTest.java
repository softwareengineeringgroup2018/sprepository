/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package studyplanner;

import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author zkv15rzu
 */
public class StudentTest {
    
    public StudentTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setName method, of class Student.
     */
    @Test
    public void testSetName() {
        System.out.println("setName() Test");
        String newName = "Bob";
        StudentProfile instance = new StudentProfile();
        instance.setName(newName);
        if (instance.getName() != newName) {
            fail("Failed: it does not set name");
        }
          else {
            System.out.println("Name succesfully set");
        }
        
        
    }
    @Test 
    public void testGetName() {
        System.out.println("getName() Test");
        String newName = "Test";
        StudentProfile instance = new StudentProfile();
        instance.setName(newName);
        if(instance.getName() != newName) {
            fail("Failed: cannot retrieve name");
        }
        else {
            System.out.println("Name Succesfully returned");
        }
    }
    /**
     * Test of setUserID method, of class Student.
     */
    
    @Test
    public void testSetUserID() {
        System.out.println("setUserID() Test");
        String newUserID = "1";
        StudentProfile instance = new StudentProfile();
        instance.setUserID(newUserID);
        if (instance.getUserID() != newUserID) {
            fail("Failed: it does not set UserID");
        }
        else {
            System.out.println("user ID succesfully set");
        }
        
    }
    
    @Test 
    public void testGetUserID() {
        System.out.println("getName() Test");
        String newName = "Test";
        StudentProfile instance = new StudentProfile();
        instance.setUserID(newName);
        if(instance.getUserID() != newName) {
            fail("Failed: cannot retrieve UserID");
        }
        else {
            System.out.println("UserID Succesfully returned");
        }
    }
    /**
     * Test of setPassword method, of class Student.
     */
    @Test
    public void testSetPassword() {
        System.out.println("setPassword");
        String newPassword = "password";
        StudentProfile instance = new StudentProfile();
        instance.setPassword(newPassword);
        if (instance.getPassword() != newPassword) {
            fail("Failed: it does not set UserID");
        }
        else {
            System.out.println("Password succesfully test");
        }
       
    }
    
     @Test 
    public void testGetPassword() {
        System.out.println("getName() Test");
        String newPassword = "Test";
        StudentProfile instance = new StudentProfile();
        instance.setPassword(newPassword);
        if(instance.getPassword() != newPassword) {
            fail("Failed: cannot retrieve Password");
        }
        else {
            System.out.println("UserID Succesfully returned");
        }
    }
}
