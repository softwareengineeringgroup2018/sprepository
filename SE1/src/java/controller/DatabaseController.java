
package controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import studyplanner.StudentProfile;

/**
 *
 * @author ngx16ybu
 */
public class DatabaseController {

    static final String WRITE_STUDENT_SQL = "INSERT INTO sql2238034.student_db(userID, firstName, lastName, email, password, student) VALUES (?, ?, ?, ?, ?, ?)";
    static final String LOAD_STUDENT_SQL = "SELECT student FROM sql2238034.student_db WHERE userID =?";
    static final String UPDATE_STUDENT_SQL = "UPDATE sql2238034.student_db SET student =? WHERE userID =?";
    static final String UPDATE_EMAIL_SQL = "UPDATE sql2238034.student_db SET email =? WHERE userID =?";
    static final String UPDATE_FNAME_SQL = "UPDATE sql2238034.student_db SET firstname =? WHERE userID =?";
    static final String UPDATE_LNAME_SQL = "UPDATE sql2238034.student_db SET lastName =? WHERE userID =?";
    static final String UPDATE_PASSWORD_SQL = "UPDATE sql2238034.student_db SET password =? WHERE userID =? AND password=?";
    static final String MAKE_TABLE_SQL = "CREATE TABLE sql2238034.student_db ( "
            + "userID VARCHAR(8)UNIQUE,"
            + "firstName VARCHAR(30),"
            + "lastName VARCHAR(30),"
            + "email VARCHAR(40),"
            + "password VARCHAR(30),"
            + "student blob )";

    ;
    
    public DatabaseController() {

    }

    public void makeTable() throws ClassNotFoundException, SQLException {
        Connection conn = this.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(MAKE_TABLE_SQL);
        pstmt.execute();
    }

    public static Connection getConnection() throws ClassNotFoundException {
        String driver = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://sql2.freemysqlhosting.net";
        String username = "sql2238034";
        String password = "mP4*cN4!";
        Class.forName(driver);
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, username, password);
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return conn;
    }

    public String writeStudentProfile(String userID, String fName, String lName, String email, String password) throws Exception {
        Connection conn = this.getConnection();

        PreparedStatement pstmt = conn.prepareStatement(WRITE_STUDENT_SQL);
        StudentProfile sp = new StudentProfile(userID);
        // set input parameters
        pstmt.setString(1, userID);
        pstmt.setString(2, fName);
        pstmt.setString(3, lName);
        pstmt.setString(4, email);
        pstmt.setString(5, password);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);

        oos.writeObject(sp);
        oos.flush();
        oos.close();
        bos.close();

        byte[] nstu = bos.toByteArray();
        pstmt.setObject(6, nstu);
        pstmt.executeUpdate();

        pstmt.close();
        System.out.println("Saved Student Data" + userID);
        return userID;
    }

    public void updateStudentProfile(String userID, StudentProfile student) throws SQLException, IOException, ClassNotFoundException {
        Connection conn = this.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(UPDATE_STUDENT_SQL);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(student);
        oos.flush();
        oos.close();
        bos.close();
        byte[] stu = bos.toByteArray();
        pstmt.setObject(1, stu);
        pstmt.setString(2, userID);

        pstmt.executeUpdate();
        pstmt.close();
        System.out.println("Updated Student Value");

    }

    public void updateEmail(String email, StudentProfile student) throws SQLException, IOException, ClassNotFoundException {
        String userID = student.getUserID();
        Connection conn = this.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(UPDATE_EMAIL_SQL);
        pstmt.setObject(1, email);
        pstmt.setString(2, userID);
        pstmt.executeUpdate();
        pstmt.close();
        System.out.println("Updated email");
    }

    public void updatePassword(String currentPW, String newPW, StudentProfile student) throws SQLException, IOException, ClassNotFoundException {

        String userID = student.getUserID();
        Connection conn = this.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(UPDATE_PASSWORD_SQL);
        pstmt.setObject(1, newPW);
        pstmt.setString(2, userID);
        pstmt.setString(3, currentPW);
        pstmt.executeUpdate();
        pstmt.close();
    }

    public void deleteStudent(String userID) throws SQLException, ClassNotFoundException {
        Connection conn = this.getConnection();
        PreparedStatement pstmt = conn.prepareStatement("DELETE FROM sql2238034.student_db WHERE userID =?");
        pstmt.setString(1, userID);
        pstmt.executeUpdate();
    }

    public void updateFName(String fName, StudentProfile student) throws SQLException, IOException, ClassNotFoundException {
        String userID = student.getUserID();
        Connection conn = this.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(UPDATE_FNAME_SQL);
        pstmt.setObject(1, fName);
        pstmt.setString(2, userID);
        pstmt.executeUpdate();
        pstmt.close();
        System.out.println("Updated first name");
    }

    public void updateLName(String lName, StudentProfile student) throws SQLException, IOException, ClassNotFoundException {
        String userID = student.getUserID();
        Connection conn = this.getConnection();
        PreparedStatement pstmt = conn.prepareStatement(UPDATE_LNAME_SQL);
        pstmt.setString(1, lName);
        pstmt.setString(2, userID);
        pstmt.executeUpdate();
        pstmt.close();
        System.out.println("Updated last name");
    }

    public StudentProfile readStudentProfile(String id) throws Exception {
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        StudentProfile newStu = null;
        Connection conn = this.getConnection();

        pstmt = conn.prepareStatement(LOAD_STUDENT_SQL);
        pstmt.setString(1, id);
        rs = pstmt.executeQuery();

        if (rs.next()) {
            ByteArrayInputStream bais;
            ObjectInputStream ins;

            try {
                bais = new ByteArrayInputStream(rs.getBytes("student"));
                ins = new ObjectInputStream(bais);
                newStu = (StudentProfile) ins.readObject();

                System.out.println(newStu.getUserID() + "has been loaded");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        rs.close();
        pstmt.close();

        return newStu;
    }

}
