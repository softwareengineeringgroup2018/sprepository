/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import studyplanner.*;

/**
 *
 * @author ngx16ybu
 */
@WebServlet(name = "TaskController", urlPatterns = {"/TaskController"})
public class TaskController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String taskname = request.getParameter("taskname");
        String descrip = request.getParameter("description");
        String s = request.getParameter("duration");
        Long d = Long.parseLong(s);
        int priority = Integer.valueOf(request.getParameter("priority"));
        String module = request.getParameter("module");
        String assignment = request.getParameter("assignment");
        
        HttpSession session = request.getSession();
        String userID = session.getAttribute("name").toString();
        PrintWriter out = response.getWriter();
        DatabaseController db = new DatabaseController();
        try {
            int x = 0;
            
            StudentProfile stu = db.readStudentProfile(userID);
                for (Module m : stu.getModules()) {
                     for (Assignment A : m.getAssignments()) {
                    if (A.getName().equalsIgnoreCase(assignment)) {
                        Task t = new Task(taskname, descrip, d, priority, assignment);
                        A.addTask(t);
                        x++;
                    }
                     }
        
            }
     db.updateStudentProfile(userID, stu);
      

        } catch (Exception ex) {
            Logger.getLogger(TaskController.class.getName()).log(Level.SEVERE, null, ex);
        }
        RequestDispatcher rd = request.getRequestDispatcher("/tasks.jsp");
            rd.forward(request, response);
        out.close();

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}

/**
 * Returns a short description of the servlet.
 *
 * @return a String containing servlet description
 */
