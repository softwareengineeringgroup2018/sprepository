/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package studyplanner;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author ging3
 */
public class Semester implements Serializable {

    Date startDate;
    Date endDate;
    Double semesterProgress;
    SimpleDateFormat format;
    private static final long serialVersionUID = 1L;

    public Semester() {
        format = new SimpleDateFormat("dd/MM/yy");
        startDate = null;
        startDate = null;
        semesterProgress = 0.0;
    }

    public Semester(String startDate, String endDate) throws ParseException {
        format = new SimpleDateFormat("dd/MM/yy");
        try {
            this.startDate = format.parse(startDate);
            this.endDate = format.parse(endDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        semesterProgress = 0.0;
    }

    public void setStartDate(String startDate) {
        try {
            this.startDate = format.parse(startDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void setEndDate(String endDate) {
        try {
            this.endDate = format.parse(endDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void setProgress(Double progress) {
        this.semesterProgress = progress;
    }

    /* 
    * Get Method for "endDate" 
    * returns endDate
     */
    public String getEndDate() {
        return format.format(endDate);
    }

    /* 
    * Get Method for "semesterProgress" 
    * returns semesterProgress
     */
    public Double getProgress() {
        return this.semesterProgress;
    }

    /* 
    * Get Method for "startDate" 
    * returns startDate
     */
    public String getStartDate() {
        return format.format(startDate);
    }

    @Override
    public String toString() {
        return "Semester Time Period '" + this.getStartDate() + "' - '" + this.getEndDate() + "\n"
                + "Semester Progress = " + this.semesterProgress + "%";
    }

}
