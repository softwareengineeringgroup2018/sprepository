package studyplanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author zkv15rzu
 */
import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 *
 * @author ngx16ybu
 */
public abstract class Assignment implements Serializable {

    String name;
    int weight;
    double progress;
    Date date;
    int grade;
    boolean marked;
    boolean generated;
    private static final long serialVersionUID = 1L;
    List<Task> tasks;

    //Get Methods
    /**
     * Accessor Method
     *
     * @return name of assignment
     */
    public String getName() {
        return this.name;
    }

    /**
     * Accessor Method
     *
     * @return weight of assignment
     */
    public int getWeight() {
        return weight;
    }

    /**
     * Accessor Method
     *
     * @return progress of assignment
     */
    public double getProgress() {
        return progress;
    }

    /**
     * Accessor Method
     *
     * @return date of assignment
     */
    public Date getDate() {
        return date;
    }

    /**
     * Accessor Method
     *
     * @return grade of assignment
     */
    public int getGrade() {
        return this.grade;
    }

    /**
     * Accessor Method
     *
     * @return true if marked false if not
     */
    public boolean isMarked() {
        return this.marked;
    }

    //set methods 
    /**
     * Mutator Method sets name of assignment
     *
     * @param name String
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Mutator Method sets weight of assignment
     *
     * @param weight int
     *
     */
    public void setWeight(int weight) {
        this.weight = weight;
    }

    /**
     * Mutator Method sets progress of assignment
     *
     * @param progress double
     */
    public void setProgres(double progress) {
        this.progress = progress;
    }

    /**
     * Mutator Method sets date of assignment
     *
     * @param date Date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Mutator Method sets grade of assignment
     *
     * @param grade int
     */

    public void setGrade(int grade) {
        this.grade = grade;
        this.marked = true;
    }

    /**
     * Accessor Method
     *
     * @return list of tasks
     */
    public List<Task> getTasks() {
        return this.tasks;
    }

    /**
     * Add method Task, adds task to tasks
     */
    public void addTask(Task T) {
        this.tasks.add(T);
    }

    /**
     * sets a task in the arraylist to complete
     */
    public void completeTask(Task T) {
        for (Task t : tasks) {
            if (t.equals(T)) {
                T.CompleteTask();
            }
        }
    }

    /**
     * sets a task completion status to false
     */
    public void undoCompleteTask(Task T) {
        for (Task t : tasks) {
            if (t.equals(T)) {
                T.UndoCompleteTask();
            }
        }
    }

    public abstract void generateAssignmentTasks();

    /**
     * Algorithim which generates the priority for the tasks in the assignment
     * based on the day difference
     */
    public void prorityGenerator() {

        Date today = new Date();
        long diff = (this.getDate().getTime() - today.getTime()) / (1000 * 60 * 60 * 24);
        diff = diff / 2;
        for (int i = 0; i < tasks.size(); i++) {
            tasks.get(i).setPriority((int) (diff + i));
        }
    }

}

/**
 * Comparator used in sorting Assignments by due date
 */

class DateComparator implements Comparator<Assignment> {

    @Override
    public int compare(Assignment A, Assignment A2) {
        if (A.getDate().before(A2.getDate())) {
            return -1;
        } else if (A.getDate().after(A2.getDate())) {
            return 1;
        } else {
            return 0;
        }
    }

}
