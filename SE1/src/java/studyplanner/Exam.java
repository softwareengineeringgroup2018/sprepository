package studyplanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author zkv15rzu
 */
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;

public class Exam extends Assignment {

    private String location;
    private Duration duration;

    Exam() {

    }

    Exam(String name, int weight, String location, long duration, Date date) {
        this.name = name;
        this.weight = weight;
        this.date = date;
        this.location = location;
        this.duration = Duration.ofMinutes(duration);
        this.progress = 0;
        this.grade = 0;
        this.tasks = new ArrayList<>();
        this.generated = false;
    }

    /**
     * Accessor Method
     *
     * @return location of exam
     */
    public String getLocation() {
        return location;
    }

    /**
     * Accessor Method
     *
     * @return date of assignment
     */
    public String getDuration() {

        String s = "Duration: " + this.duration.toHours() + " Hours ";
        if (this.duration.minusHours(this.duration.toHours()).toMinutes() == 0) {
            return s;
        }
        return s += this.duration.minusHours(this.duration.toHours()).toMinutes() + " Minutes";

    }

    /**
     * Mutator Method sets location of exam
     *
     * @param location String
     *
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * Mutator Method sets duration of exam
     *
     * @param duration long
     */
    public void setDuration(long duration) {
        this.duration = Duration.ofMinutes(duration);
    }

    @Override
    public String toString() {
        return "Exam Name: " + this.getName() + "\n"
                + "Weight: " + this.getWeight() + "\n"
                + "Submission Date: " + this.getDate() + "\n"
                + "Location: " + this.location + "\n"
                + "Duration: " + this.getDuration() + "\n";
    }

    /**
     * Generates tasks based on the weight of the assignment
     */
    @Override
    public void generateAssignmentTasks() {
        double d = this.getWeight() / 10 * 2;
        for (int i = 1; i < d + 1; i++) {
            String str = "Studying Task " + Integer.toString(i);
            Task t = new Task(this.getName(), str, 150, 0, this.getName());
            this.tasks.add(t);
        }
        this.generated = true;
        this.prorityGenerator();
    }

}
