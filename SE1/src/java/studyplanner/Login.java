package studyplanner;

import static controller.DatabaseController.getConnection;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Login implements Serializable {

    public static boolean validate(String name, String pass) {

        Connection conn = null;
        PreparedStatement Pst = null;

        try {
            conn = getConnection();

            Pst = conn.prepareStatement("select userID,password from sql2238034.student_db where userID=? and password=?");
            Pst.setString(1, name);
            Pst.setString(2, pass);

            ResultSet rs = Pst.executeQuery();

            if (rs.next()) {
                return true;
            }

        } catch (SQLException e) {
            System.out.println(e);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;
    }

//    public static void main(String[] args) {
//
//        String name = "emil";
//        String pass = "emil";
//        System.out.println(Login.validate(name, pass));
//
//    }
}
