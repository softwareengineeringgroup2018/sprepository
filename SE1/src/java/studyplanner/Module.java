package studyplanner;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
public class Module implements Serializable {

    private String name;
    private String organiser;
    private String code;
    private double progress;
    private List<Assignment> Assignments;

    private static final long serialVersionUID = 1L;

    public Module() {
        this.name = "";
        this.organiser = "";
        this.code = "";
        this.Assignments = new ArrayList<>();
        this.progress = 0;
    }

    public Module(String name, String organiser, String code) {
        this.Assignments = new ArrayList<>();
        this.name = name;
        this.organiser = organiser;
        this.code = code;
        this.progress = 0;
    }

    /**
     * Mutator Method sets name of Module
     *
     * @param name String
     *
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Mutator Method sets organiser of Module
     *
     * @param organiser String
     *
     */
    public void setOrganiser(String organiser) {
        this.organiser = organiser;
    }

    /**
     * Mutator Method sets code of Module
     *
     * @param code String
     *
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Mutator Method sets progress of Module
     *
     * @param double progress
     *
     */
    public void setProgress(double progress) {
        this.progress = progress;
    }

    /**
     * Accessor Method
     *
     * @return name of module
     */
    public String getName() {
        return name;
    }

    /**
     * Accessor Method
     *
     * @return list of assignments of module
     */
    public List<Assignment> getAssignments() {
        return this.Assignments;
    }

    /**
     * Accessor Method
     *
     * @return Organiser of module
     */
    public String getOrganiser() {
        return organiser;
    }

    /**
     * Accessor Method
     *
     * @return code of module
     */
    public String getCode() {
        return code;
    }

    /**
     * Accessor Method
     *
     * @return progress of module
     */
    public double getProgress() {
        return progress;
    }

    /**
     * adds assignment to the module class
     */
    public void addAssignment(Assignment assignment) {
        Assignments.add(assignment);
    }

    /**
     * prints out assignments for testing purposes
     */
    public void printAssignments() {
        for (Assignment A : Assignments) {
            System.out.println("Assignment Name: " + A.getName());
            System.out.println("Weight: " + A.getWeight() + "%");
            System.out.println("Progress: " + A.getProgress() + "%");
            System.out.println("Date: " + A.getDate() + "\n");
            System.out.println(A.getClass().getSimpleName());

        }
    }

    /**
     * calculates the progress of the user depending on the weight of completed
     * assignments
     */
    public void calculateProgress() {
        double i = 0;
        double t = 0;
        for (Assignment A : Assignments) {
            i++;
            t = t + A.getProgress();
        }
        this.progress = (t / i);
    }

    @Override
    public String toString() {
        return this.name;
    }

}
