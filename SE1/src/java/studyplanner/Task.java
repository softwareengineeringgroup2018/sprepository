/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package studyplanner;

import java.io.Serializable;
import java.time.Duration;
import java.util.Comparator;

/**
 *
 * @author zkv15rzu
 */
public class Task implements Serializable {

    String name;
    String type;
    int priority;
    private Duration duration;
    boolean completed;
    private String aName;
    private static final long serialVersionUID = 1L;
    
    public Task(){
        
    }
    

    public Task(String name, String type, long time, int priority, String aName) {
        this.name = name;
        this.type = type;
        this.duration = Duration.ofMinutes(time);
        this.aName = aName;

        this.priority = priority;
        this.completed = false;
    }

    //name get and set
    public String getName() {
        return this.name;
    }

    public void setName(String s) {
        this.type = s;
    }

    public String getAName() {
        return this.aName;
    }

    public void setAName(String aName) {
        this.aName = aName;
    }
    //type set and get

    public String getType() {
        return this.type;
    }

    public void setType(String s) {
        this.type = s;
    }

    //timerequired set and get
    public String getTimeRequired() {
        String s = this.duration.toHours() + " Hours ";
        if (this.duration.minusHours(this.duration.toHours()).toMinutes() == 0) {
            return s;
        }
        return s += this.duration.minusHours(this.duration.toHours()).toMinutes() + " Minutes";
    }
    //priority set and get

    public int getPriority() {
        return this.priority;
    }

    public void setPriority(int i) {
        this.priority = i;
    }

    //completion and completetask 
    public boolean getCompletion() {
        return this.completed;
    }

    public void CompleteTask() {
        this.completed = true;
    }

    public void UndoCompleteTask() {
        this.completed = false;
    }

    @Override
    public String toString() {
        return this.type + " for " + this.name + " | TimeRequired: "
                + this.getTimeRequired() + "\n";

    }

}

class PriorityComparator implements Comparator<Task> {

    @Override
    public int compare(Task t, Task t1) {
        return Integer.compare(t.getPriority(), t1.getPriority());

    }
}
