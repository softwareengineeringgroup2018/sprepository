/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package studyplanner;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author zkv15rzu
 */
public class StudentProfile implements Serializable {

    private String userID;
    private List<Module> modules;
    private Semester semester;
    private static final long serialVersionUID = 1L;

    //default constructor
    public StudentProfile() {
        userID = null;
        modules = new ArrayList<>();
        semester = new Semester();

    }

    public StudentProfile(String userID) {
        this.userID = userID;
        modules = new ArrayList<>();
        semester = new Semester();

    }

    /**
     * Mutator Method sets userID of Student
     *
     * @param newUserID String
     */
    public void setUserID(String newUserID) {
        userID = newUserID;
    }

    /**
     * Accessor Method
     *
     * @return UserID
     */
    public String getUserID() {
        return this.userID;
    }

    /**
     * adds module to the student object
     */
    public void addModule(Module M) {
        modules.add(M);
    }

    /**
     * Accessor Method
     *
     * @return module list
     */
    public List<Module> getModules() {
        return this.modules;
    }

    /**
     * Accessor Method
     *
     * @return semester object
     */
    public Semester getSemester() {
        return this.semester;
    }

    /**
     * Mutator Method sets Semester of Student
     *
     * @param Semester Semester
     */
    public void setSemester(Semester semester) {
        this.semester = semester;
    }

    /**
     * Algorithm to calculate total progress saved in semester
     * (semesterprogress) it is calculated by getting the average of module
     * progress
     */
    public void calculateTotalProgress() {
        double i = 0;
        double t = 0;
        for (Module M : modules) {
            i++;
            System.out.println(i);
            t = t + M.getProgress();
            System.out.println(t);
        }
        System.out.println(i);
        this.semester.setProgress(t / i);
    }

    /**
     * @return list of tasks which have not been completed sorted by priority
     */
    public List<Task> allTasksToDo() {
        List<Task> Tasks = new ArrayList<>();
        for (Module M : modules) {
            for (Assignment A : M.getAssignments()) {
                for (Task T : A.getTasks()) {
                    if (!T.completed) {
                        Tasks.add(T);
                    }
                }
            }
        }
        Collections.sort(Tasks, new PriorityComparator());
        return Tasks;
    }

    /**
     * @returns module which name matches the input string
     */
    public Module getModule(String name) {
        Module ret = new Module();
        for (Module M : modules) {
            if (M.getName().equalsIgnoreCase(name)) {
                ret = M;

            }
        }
        return ret;
    }

    /**
     * Generates the tasks for all assignments
     */
    public void generateTasks() {
        for (Module Mod : modules) {
            for (Assignment A : Mod.getAssignments()) {
                if (!A.generated) {
                    A.generateAssignmentTasks();
                }
            }

        }
    }

    /**
     * removes all tasks for all assignments
     */
    public void clearTasks() {
        for (Module mod : modules) {
            for (Assignment A : mod.getAssignments()) {
                for (Task t : A.getTasks()) {

                }
                A.generated = false;
            }
        }
    }

//     /**
//     * removes all tasks for all assignments 
//     */
//    public void deleteTask(String mName, String aName, String taskName){
//        for (Module mod : modules) {
//            if (mod.getName().equalsIgnoreCase(mName)) {
//                for (Assignment A : mod.getAssignments()) {
//                    
//                }
//            }
//        }
//    }
    /**
     * @returns List of Assignments sorted by date
     */
    public List<Assignment> upcommingDeadlines() {
        List<Assignment> deadlines = new ArrayList<>();
        for (Module M : modules) {
            for (Assignment A : M.getAssignments()) {
                deadlines.add(A);

            }
        }
        Collections.sort(deadlines, new DateComparator());

        return deadlines;
    }

    /**
     * populates modules and there respective assignments through a file
     */
    public void uploadTxt(String file) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(file));
        scanner.useDelimiter("\n");
        ArrayList<String> moduleInfo = new ArrayList<>();
        String s;
        while (scanner.hasNext()) {
            s = scanner.next();
            s = s.trim();
            s = s.toLowerCase();
            //System.out.println(s);
            moduleInfo.add(s);
        }
        int x = 0;
        while (x < moduleInfo.size()) {
            String[] parts = moduleInfo.get(x).split(",");
            //System.out.println(parts[3]);
            Module newModule = new Module(parts[0], parts[1], parts[2]);
            x++;
            int counter = Integer.parseInt(parts[3]);

            for (int i = 0; i < counter; i++) {
                String[] assigns = moduleInfo.get(x).split(",");
                if ("exam".equals(assigns[0])) {
                    //System.out.println("exam " + assigns[1] + " " + assigns[2] + " " + assigns[3] + " " + assigns[4]+ " " + assigns[5]);
                    Exam ex = new Exam(assigns[1], (int) Integer.parseInt(assigns[2]),
                            assigns[3], (int) Integer.parseInt(assigns[4]), Date.valueOf(assigns[5]));
                    newModule.addAssignment(ex);
                    x++;
                }
                if ("coursework".equals(assigns[0])) {
                    //System.out.println("coursework " + assigns[1] + " " + assigns[2] + " " + assigns[3]);
                    Coursework CW = new Coursework(assigns[1], (int) Double.parseDouble(assigns[2]), Date.valueOf(assigns[3]), assigns[4]);
                    newModule.addAssignment(CW);
                    x++;
                }

            }

            this.addModule(newModule);

        }
        scanner.close();
//        file.delete();
    }

}
