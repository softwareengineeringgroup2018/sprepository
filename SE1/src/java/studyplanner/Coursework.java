package studyplanner;

import java.util.ArrayList;
import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author zkv15rzu
 */
public class Coursework extends Assignment {

    public int extension;
    public String details;
    private static final long serialVersionUID = 1L;

    Coursework() {

    }

    Coursework(String name, int weight, Date date, String details) {
        this.name = name;
        this.weight = weight;
        this.date = date;
        this.extension = 0;
        this.progress = 0;
        this.details = details;
        this.grade = 0;
        this.marked = false;
        this.generated = false;
        this.tasks = new ArrayList<>();

    }

    /**
     * Accessor Method
     *
     * @return extension if applicable
     */
    public int getExtension() {
        return extension;
    }

    /**
     * Accessor Method
     *
     * @return details of the coursework
     */
    public String getDetails() {
        return this.details;
    }

    /**
     * Mutator Method sets extension of assignment
     *
     * @param extension - int
     */
    public void setExtension(int extension) {
        this.extension = extension;
    }

    /**
     * Mutator Method sets details of assignment
     *
     * @param details - String
     */
    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public String toString() {
        return "CourseWork Name: " + this.getName() + "\n"
                + "Weight: " + this.getWeight() + "\n"
                + "Submission Date: " + this.getDate() + "\n"
                + "Coursework Details: " + this.details + "\n";
    }

    /**
     * Generates tasks based on the weight of the assignment
     */
    @Override
    public void generateAssignmentTasks() {
        double d = this.getWeight() / 10 * 2;
        for (int i = 1; i < d + 1; i++) {
            String str = "Programming task " + Integer.toString(i);
            Task t = new Task(this.getName(), str, 150, 0, this.getName());
            this.tasks.add(t);

        }
        this.generated = true;
        this.prorityGenerator();
    }
}
